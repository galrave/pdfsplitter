# Pdf Bundle splitter #

This JAR project is used to split the bundled PDF produced by אפי into
the individual paychecks it contains.

The project is hosted on [Bitbucket](https://bitbucket.org/benjamin_albert/pdf-splitter) and is versiond using [Git](https://git-scm.com/).

### How do I get set up? ###

* Open to the [projects page](https://bitbucket.org/benjamin_albert/pdf-splitter) in your browser.

* Clone the repository onto your local machine (press the clone button on the sidebar).
* [Download maven](https://maven.apache.org/download.cgi) and [install it](http://www.mkyong.com/maven/how-to-install-maven-in-windows/).
* Run the `mvn install` command in the project directory to download the projects dependencies and run unit tests.

* Add the project to eclipse [(instructions here)](http://stackoverflow.com/questions/2636201/how-to-create-a-project-from-existing-source-in-eclipse-and-then-find-it)