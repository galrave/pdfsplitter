package com.ladpc.pdfsplitter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

/**
 * Parses a PDF files bookmarks into a {@link Collection} 
 * of {@link Unit}s. <p>
 * 
 * For details on how the individual unit's are parsed 
 * and determined see {@link #parse(PdfReader)}. 
 * 
 * @author Benjamin Albert
 * @since  26th Nov  2015
 */
public class BookmarkParser {
    // A regular expression that selects the first number at the beginning
    // of the line.
    //
    // For example if the input is "1234 XYX 5698 dad" this pattern will
    // select 1234.
    // NOTE: If the input string contains number NOT at the beginning of the line
    // (INCLUDING WHITESPACE) this pattern will not select that number.
    // for example " 1234" will not select the first number.
    protected final Pattern numberAtBeginningOfLine = Pattern.compile("^\\d+");
    
    public Collection<Unit> parse(String filePath) throws IOException, ParseException {
        return parse(new FileInputStream(filePath));
    }
    
    public Collection<Unit> parse(InputStream in) throws IOException, ParseException {
        return parse(new PdfReader(in));
    }
    
    /**
     * Parses a bundled PDF file's bookmarks and determines the individual
     * {@link Unit}s (documents) it contains. <p>
     * 
     * A unit's id is determined by the number at the beginning of the line
     * of a the <b>bookmark's title</b>. <p>
     * 
     * Bookmark's that there title doesn't start with a number will be skipped
     * (not returned in the {@link Collection} of units). <p>
     * 
     * A unit's first page number is determined by the <b>bookmark's page number</b>. <br>
     * A unit's last page number is determined by <b>next bookmark's page number</b>
     * or the documents page count if there is no next bookmark. <p>
     * 
     * ��� ����� says that we can relay on the ids to be unique in the bundle
     * so I don't need to test for duplicate ids. <br>
     * I have a feeling we'll regret this especially when this is something 
     * trivial to implement.
     * 
     * @param reader    A {@link PdfReader} object to read the bookmarks from.
     * @return          A {@link Collection} of {@link Unit} objects that 
     *                  represent the individual documents inside the bundles
     *                  PDF file. 
     *       
     * @throws ParseException    Thrown if the PDF file doesn't contain valid bookmarks. 
     */
    public Collection<Unit> parse(final PdfReader reader) throws ParseException {
        @SuppressWarnings("unchecked")
        final List<Map<String, Object>> bookmarks = 
                SimpleBookmark.getBookmark(reader);
        
        if (bookmarks == null) {
            throw new ParseException("The given pdf does not have any bookmarks");
        }
        
        Unit unit = null;
        final Collection<Unit> units = new ArrayList<Unit>( bookmarks.size() );
        for ( Map<String, Object> bookmark : bookmarks ) {            
            final String unsafePageNumber = bookmark.get("Page").toString();
            final Matcher pageMatcher = 
                    numberAtBeginningOfLine.matcher( unsafePageNumber );
            
            if ( !pageMatcher.find() ) {
                throw new ParseException("Unable to parse page number (bookmark title: " + bookmark.get("Title") + ")");
            }
            
            // If we have a previous unit set this bookmark's
            // (page number - 1) as the last page. 
            if ( unit != null ) {
                final int page = Integer.parseInt( pageMatcher.group() ) - 1;
                unit.setLastPage( page );
                unit = null;
            }
            
            final String unsafeId = bookmark.get("Title").toString();
            final Matcher idMatcher = 
                    numberAtBeginningOfLine.matcher( unsafeId );
            
            // Skip over the bookmark if it doesn't have an id.
            if ( idMatcher.find() ) {
               // Create a unit that we will assign it's last page 
               // in the next iteration using the next bookmarks page number,
               // or outside the loop assigning the document's last page.
               unit = new Unit();
               unit.setId( idMatcher.group() );
               unit.setFirstPage(Integer.parseInt( pageMatcher.group() ));
               
               units.add(unit);
            }
        }
        
        if (units.size() == 0) {
            throw new ParseException("Non of the pdf's bookmarks contain valid ids");
        }
        
        // Assign the bundles page count as the last unit's
        // last page number.
        if (unit != null) {
            unit.setLastPage( reader.getNumberOfPages() );
        }
        
        return units;
    }
}
