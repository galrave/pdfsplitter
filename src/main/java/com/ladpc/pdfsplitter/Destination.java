package com.ladpc.pdfsplitter;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Describes an object that provides and {@link OutputStream}
 * to write the individual units in the PDF bundle.
 * 
 * @author Benjamin Albert
 * @since  3rd Dec 2015
 */
public interface Destination {
    /**
     * Create and prepare an {@link OutputStream} for the given
     * {@link Unit}. <p>
     * 
     * The underlying implementation can close the previously 
     * returned {@link OutputStream}.
     * 
     * @param unit  The individual {@link Unit} to write. 
     * 
     * @return      The {@link OutputStream} to which the 
     *              individual {@link Unit}'s document should be written to.
     */
    OutputStream getOutput( Unit unit ) throws IOException;
    
    /**
     * Call once to close and release all associated resources.
     */
    void close() throws IOException;
}
