package com.ladpc.pdfsplitter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import com.lowagie.text.pdf.PdfReader;

public class FileSystemDestination implements Destination {

    private FileOutputStream currentFile;
    private String directory;
    
    public FileSystemDestination() {
        this("./");
    }
    
    public FileSystemDestination(String directory) {
        File dir = new File(directory);
        if (!dir.exists()) {
            throw new IllegalArgumentException(directory + " doesn't exist");
        }
        
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException(directory + " is not a directory");
        }
        
        this.directory = dir.getAbsolutePath() + File.separator;
    }
    
    public OutputStream getOutput(Unit unit) throws IOException {
        closeCurrentFile();
        currentFile = new FileOutputStream(directory + unit.getId() + ".pdf");
                
        return currentFile;
    }

    public void close() throws IOException {
        closeCurrentFile();
    }

    private void closeCurrentFile() throws IOException {
        if (currentFile != null) {
            currentFile.close();
        }
    }
    
    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        String inFilePath = "C:\\Users\\albertbe\\workspace\\PdfSplitter\\TLUSH629-201510.pdf";
        PdfReader reader = new PdfReader(new FileInputStream( inFilePath ));
        
        Collection<Unit> units = new BookmarkParser().parse(reader);
        
        String outputDir = "C:\\Users\\albertbe\\workspace\\PdfSplitter\\TLUSH629-201510";
//        Destination fileSystem = new FileSystemDestination(outputDir);
        Destination zipArchive = new ZipArchiveDestination(new FileOutputStream(outputDir + ".zip"));
        new Splitter(reader).split(units, zipArchive/*fileSystem*/);
        
        System.out.println(System.currentTimeMillis()-start);
    }
}
