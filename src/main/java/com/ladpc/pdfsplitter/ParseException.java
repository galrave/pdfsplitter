package com.ladpc.pdfsplitter;

/**
 * Thrown when a PDF file given to the {@link BookmarkParser}
 * doesn't contain valid bookmarks.
 * 
 * @author Benjamin Albert
 * @since  26th Nov  2015
 */
@SuppressWarnings("serial")
public class ParseException extends Exception {
    public ParseException() {
        super();
    }

    public ParseException(String message) {
        super(message);
    }
}
