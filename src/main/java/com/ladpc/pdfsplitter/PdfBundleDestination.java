package com.ladpc.pdfsplitter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.pdf.SimpleBookmark;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BadPdfFormatException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSmartCopy;

/**
 * A {@link Destination} implementation which directs all unit
 * bytes to a single .PDF file. <p>
 * 
 * This destination also takes care of shifting each
 * {#link Unit}'s bookmarks.
 * 
 * @author  Benjamin Albert
 * @since   10th Apr 2016.
 */
public class PdfBundleDestination implements Destination {
    
    // Use a PdfSmartCopy to avoid duplicating 
    // resources such as fonts and images.
    private final PdfCopy bundle;
    
    // Holds the binary of the PDF binary of
    // the current Unit's PDF document.
    private ByteArrayOutputStream buffer;
    
    // The buffers initial size.
    private final int bufferSize;
    
    // Holds the number of pages to shifts the next
    // Unit's bookmarks.
    private int pageOffset;
    
    // List containing all Unit's bookmarks after they
    // were shifted by pageOffset.
    @SuppressWarnings({ "rawtypes" })
    final private List shiftedBookmarks = new ArrayList();
    
    private static final int DEFAULT_BUFFER_SZIE = 0xffff; // 64KB. 
    
    public PdfBundleDestination(OutputStream out) throws DocumentException {
        this(out, DEFAULT_BUFFER_SZIE);
    }
    
    public PdfBundleDestination(OutputStream out, int bufferSize) throws DocumentException {
        this.bufferSize = bufferSize;
        final Document doc = new Document();
        this.bundle = new PdfSmartCopy(doc, out);
        doc.open();
    }
    
    public PdfBundleDestination(PdfCopy bundle) throws DocumentException {
        this(bundle, DEFAULT_BUFFER_SZIE);
    }
    
    public PdfBundleDestination(PdfCopy bundle, int bufferSize) throws DocumentException {
        this.bundle = bundle;
        this.bufferSize = bufferSize;
    }

    public OutputStream getOutput(Unit unit) throws IOException {
        if (buffer != null) {
            flushDoc();
        }
        
        return buffer = new ByteArrayOutputStream( bufferSize );
    }

    private void flushDoc() throws IOException {
        final PdfReader reader = new PdfReader( buffer.toByteArray() );
        
        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            try {
                bundle.addPage( bundle.getImportedPage(reader, i) );
            } catch (BadPdfFormatException e) {
                throw new RuntimeException(e);
            }
        }
        
        collectBookmarks(reader);
        
        bundle.freeReader( reader );
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void collectBookmarks(final PdfReader reader) {
        int pageCount = reader.getNumberOfPages();
        List bookmarks = SimpleBookmark.getBookmark(reader);
        if (bookmarks != null) {
            if (pageOffset != 0) {
                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
            }
            
            shiftedBookmarks.addAll(bookmarks);
        }
        
        pageOffset += pageCount;
    }

    public void close() throws IOException {
        flushDoc();
        buffer = null;
        
        if (shiftedBookmarks.size() > 0) {
            bundle.setOutlines( shiftedBookmarks );
        }
        
        bundle.close();
    }

}
