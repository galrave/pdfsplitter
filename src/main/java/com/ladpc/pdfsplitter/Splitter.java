package com.ladpc.pdfsplitter;

import java.io.IOException;
import java.util.Collection;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;

/**
 * Splits a bundled PDF file into multiple PDF files.
 * 
 * Each file is written to an {@link OutputStream}
 * provided by {@link Destination#getOutput(Unit)}.
 */
public class Splitter {
    // The bundled PDF file.
    private PdfReader bundle;

    /**
     * @param bundle    The bundled PDF file.
     */
    public Splitter(PdfReader bundle) {
        this.bundle = bundle;
    }
    
    public void split( Collection<Unit> units, Destination dest ) throws DocumentException, IOException {
        split(units, dest, 0);
    }
    
    /**
     * @param units         Describes into what documents to split the bundle into.
     * @param dest          A {@link Destination} to write the individual documents into.
     * @param pagesToTrim   The number of pages from the end of the unit to not include 
     *                      in the document. 
     */
    public void split( Collection<Unit> units, Destination dest, int pagesToTrim ) throws DocumentException, IOException {
        for (Unit unit : units) {
            Document document = new Document(bundle.getPageSizeWithRotation(1));
            PdfCopy writer = new PdfCopy(document, dest.getOutput(unit));
            writer.setCloseStream(false);

            document.open();
            
            final int lastPage = unit.getLastPage() - pagesToTrim;
            
            for (int i = unit.getFirstPage(); i <= lastPage; i++) {
                writer.addPage( writer.getImportedPage(bundle, i) );
            }
            
            
            writer.freeReader(bundle);
            writer.close();
        }

        dest.close();
    }
}
