package com.ladpc.pdfsplitter;

/**
 * A Unit represents a single document inside
 * the bundled PDF file that needs to be written
 * to it's own file. <p>
 * 
 * A unit contains the range of page numbers in the bundled 
 * PDF file that contain this unit and a unique id.
 * 
 * For details on how the information in a unit
 * object is determined see {@link BookmarkParser#parse(com.lowagie.text.pdf.PdfReader)}.
 * 
 * @author Benjamin Albert
 * @since  26th Nov  2015
 * @see BookmarkParser
 */
public class Unit {
    private int firstPage;
    private int lastPage;
    
    private String id;
    
    /**
     * @return  The first page number in the 
     *          bundled PDF file where this unit
     *          is located. 
     */
    public int getFirstPage() {
        return firstPage;
    }
    
    public void setFirstPage(int firstPage) {
        this.firstPage = firstPage;
    }
    
    /**
     * @return  The last page number in the 
     *          bundled PDF file where this unit
     *          is located. 
     */
    public int getLastPage() {
        return lastPage;
    }
    
    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    /**
     * This is not represented by a number because it
     * will just be converted back to a string when the unit
     * is writing to it's destination (for example a file). 
     * 
     * @return A numbers that uniquely identifies this unit.
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
