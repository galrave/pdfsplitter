package com.ladpc.pdfsplitter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A {@link Destination} implementation which directs all unit
 * bytes to a single .ZIP archive file containing the individual files.
 * 
 * @author Benjamin Albert
 * @since  3rd Dec 2015
 */
public class ZipArchiveDestination implements Destination {

    private ZipOutputStream zip;
    
    private String relativePath;
    
    private String extension;
    
    public ZipArchiveDestination(OutputStream out) {
        this("", out);
    }
    
    /**
     * Create a ZipArchiveDestination with .pdf as the file
     */
    public ZipArchiveDestination(String relativePath, OutputStream out) {
        this(relativePath, ".pdf", out); 
    }
    
    /**
     * @param relativePath  The path in the .zip archive. 
     * @param extension     The file extension to append to the {@link ZipEntry} name.
     * @param out           The {@link OutputStream} to write the zip data to.
     */
    public ZipArchiveDestination(String relativePath, String extension, OutputStream out) {
        this.zip = new ZipOutputStream(out);
        
        this.relativePath = relativePath;
        this.extension = extension;
    }
    
    /**
     * {@inheritDoc}
     */
    public OutputStream getOutput(Unit unit) throws IOException {
        final int size = relativePath.length() + unit.getId().length() + extension.length();
        final StringBuilder sb = new StringBuilder(size);
        
        sb.append( relativePath ).append( unit.getId() ).append( extension );
        
        final ZipEntry unitFile = new ZipEntry(sb.toString());
        zip.putNextEntry( unitFile );
        
        return zip;
    }
    
    /**
     * {@inheritDoc}
     */
    public void close() throws IOException {
        zip.close();
    }

}
