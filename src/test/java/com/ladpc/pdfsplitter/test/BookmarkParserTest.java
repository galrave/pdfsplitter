package com.ladpc.pdfsplitter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.junit.Test;

import com.ladpc.pdfsplitter.BookmarkParser;
import com.ladpc.pdfsplitter.ParseException;
import com.ladpc.pdfsplitter.Unit;

/**
 * This test case contains unit tests for the 
 * {@link BookmarkParser} class.
 * 
 * @author Benjamin Albert
 * @since  26th Nov  2015
 */
public class BookmarkParserTest {
    // This package (directory) contains the mock PDF files used
    // to for testing.
    private static final String MOCK_PACKAGE = "/com/ladpc/pdfsplitter/test/mock/";
    
    /**
     * The bundled PDF contains pages that are not 
     * associated to a worker (but provide information about 
     * the contents of the package).
     * 
     * This tests makes sure that the parser ignores those pages
     * by testing against a PDF that contains two units and two
     * bookmarks with nun-numeric titles.
     */
    @Test
    public void testParseWithIrrelevantPages() throws IOException, ParseException {
        final String PDF_FILE = MOCK_PACKAGE + "IrrelevantPages.pdf";
        final InputStream in = this.getClass().getResourceAsStream(PDF_FILE);
        
        final Iterator<Unit> iter = new BookmarkParser().parse(in).iterator();
        
        assertTrue(iter.hasNext());
        final Unit firstPdf = iter.next();
        
        assertEquals("6296297", firstPdf.getId());
        assertEquals(1, firstPdf.getFirstPage());
        assertEquals(4, firstPdf.getLastPage());
        
        assertTrue(iter.hasNext());
        final Unit secondPdf = iter.next();
        
        assertEquals("35952605", secondPdf.getId());
        assertEquals(5, secondPdf.getFirstPage());
        assertEquals(6, secondPdf.getLastPage());
        
        assertFalse(iter.hasNext());
    }
}
