package com.ladpc.pdfsplitter.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.Test;

import com.ladpc.pdfsplitter.Destination;
import com.ladpc.pdfsplitter.PdfBundleDestination;
import com.ladpc.pdfsplitter.Unit;
import com.ladpc.pdfsplitter.ZipArchiveDestination;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.SimpleBookmark;
import com.lowagie.text.pdf.parser.PdfTextExtractor;

import static org.mockito.Mockito.*;

/**
 * Tests the main {@link Destination} interface implementations.
 * 
 * @author Benjamin Albert
 * @since  3rd Dec 2015
 */
public class DestinationTest {
    // Reads an input stream into a byte array. 
    private static byte[] readFully(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[10];
        
        while ( (nRead = is.read(data, 0, data.length)) != -1 ) {
          buffer.write(data, 0, nRead);
        }

        return buffer.toByteArray();
    }
    
    @Test
    public void testZipArchive() throws IOException {
        // Create a ZipArchiveDestination that writes to a 
        // byte array in memory.
        ByteArrayOutputStream zipFile = spy( new ByteArrayOutputStream() ); 
        Destination dest = new ZipArchiveDestination( "foo/", zipFile );
        
        // Create mock units.
        final Unit FIRST_UNIT = new Unit();
        FIRST_UNIT.setId("1");
        
        final byte[] FIRST_DOCUMENT = new byte[] { 1, 2, 3 };
        
        final Unit SECOND_UNIT = new Unit();
        SECOND_UNIT.setId("2");
        
        final byte[] SECOND_DOCUMENT = new byte[] { 4, 5, 6, 7, 8 };
        
        // Write the mock units to the Destination.
        final OutputStream firstOut = dest.getOutput( FIRST_UNIT );
        firstOut.write( FIRST_DOCUMENT );
        
        final OutputStream secondOut = dest.getOutput( SECOND_UNIT );
        secondOut.write( SECOND_DOCUMENT );
        
        verify(zipFile, never()).close();
        
        dest.close();
        
        verify(zipFile).close();
        
        // Read the zip archive from the in memory byte array.
        ByteArrayInputStream bis =  new ByteArrayInputStream(zipFile.toByteArray());
        ZipInputStream zis = new ZipInputStream( bis );
        
        // Make sure we only get the expected unit files.
        ZipEntry entry = null;
        int count = 0;
        while( (entry = zis.getNextEntry()) != null ) {
            final String name = entry.getName();
            if ( name.equals("foo/1.pdf") ) {
                assertArrayEquals(FIRST_DOCUMENT, readFully(zis));
            } else if ( name.equals("foo/2.pdf") ) {
                assertArrayEquals(SECOND_DOCUMENT, readFully(zis));
            } else {
                fail("Unexpected entry name: " + name);
            }
            
            count++;
        }
        
        assertEquals(2, count);
    }
    
    @Test
    public void testPdfBundle() throws IOException, DocumentException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(); 
        Destination pdfBundle = new PdfBundleDestination(buffer);
        
        // Create mock units.
        final Unit FIRST_UNIT = new Unit();
        FIRST_UNIT.setId("1");
        
        // Write a 2 page document to the destination.
        createMockDocument(pdfBundle, FIRST_UNIT, 2);
        
        // Write another 1 page document to the destination.
        final Unit SECOND_UNIT = new Unit();
        SECOND_UNIT.setId("2");
        createMockDocument(pdfBundle, SECOND_UNIT, 1);
        
        pdfBundle.close();
        
        // Read the PDF bundle created by the destination
        // and make sure it has the expected number of pages.
        PdfReader reader = new PdfReader(buffer.toByteArray());
        assertEquals(3, reader.getNumberOfPages());
        
        PdfTextExtractor extractor = new PdfTextExtractor(reader);            
        
        // Make sure all pages are present in the expected order.
        String text = extractor.getTextFromPage(1).trim();
        assertEquals("Page 1 in unit 1", text);
        
        text = extractor.getTextFromPage(2).trim();
        assertEquals("Page 2 in unit 1", text);
        
        text = extractor.getTextFromPage(3).trim();
        assertEquals("Page 1 in unit 2", text);
        
        // Makes sure the bookmarks were reconstructed as expected.
        List<HashMap<String, Object>> bookmarks = SimpleBookmark.getBookmark(reader);
        assertEquals(3, bookmarks.size());
        
        for (int i = 1; i <= bookmarks.size(); i++) {
            String pageNum = bookmarks.get(i-1).get("Page").toString();
            assertEquals(i, Integer.parseInt( pageNum.charAt(0)+"" ));
        }
    }

    private void createMockDocument(Destination pdfBundle, final Unit unit, int pages)
            throws IOException, DocumentException {
        OutputStream out = pdfBundle.getOutput(unit);
        
        Document doc1 = new Document();
        PdfWriter writer = PdfWriter.getInstance(doc1, out);
        List<HashMap<String, Object>> outlines = new ArrayList<HashMap<String, Object>>();
        
        doc1.open();
        for (int i = 1; i <= pages; i++) {
            // Add some text to the page.
            doc1.add(new Phrase("Page " + i + " in unit " + unit.getId()));

            // Add a bookmark for the page.
            HashMap<String, Object> bookmark = new HashMap<String, Object>();
            bookmark.put("Title", unit.getId());
            bookmark.put("Action", "GoTo");
            bookmark.put("Page", String.format("%d Fit", i));
            outlines.add(bookmark);
            
            doc1.newPage(); // The last blank page is ignored.
        }
        
        
        writer.setOutlines(outlines);
        
        doc1.close();
    }

}
