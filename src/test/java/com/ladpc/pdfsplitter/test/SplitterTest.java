package com.ladpc.pdfsplitter.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.ladpc.pdfsplitter.Destination;
import com.ladpc.pdfsplitter.ParseException;
import com.ladpc.pdfsplitter.Splitter;
import com.ladpc.pdfsplitter.Unit;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.parser.PdfTextExtractor;

public class SplitterTest {

    private PdfReader bundleReader;
    private Destination dest;
    private Map<String, ByteArrayOutputStream> individualFiles;
    
    // Create mock units.
    private final Unit FIRST_UNIT = new Unit();
    {
        FIRST_UNIT.setId("1");
        FIRST_UNIT.setFirstPage(2);
        FIRST_UNIT.setLastPage(3);
    }
    
    private final Unit SECOND_UNIT = new Unit();
    {
        SECOND_UNIT.setId("2");
        SECOND_UNIT.setFirstPage(6);
        SECOND_UNIT.setLastPage(8);
    }
    
    @Before
    public void setup() throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
        Document bundle = new Document();
        PdfWriter writer = PdfWriter.getInstance(bundle, bos);
        bundle.open();
        
        // https://github.com/benjamin-albert
        bundle.add(new Phrase("Fork me on GitHub"));
        
        bundle.newPage();
        bundle.add(new Phrase("Page 1 in unit 1"));
        
        bundle.newPage();
        bundle.add(new Phrase("Page 2 in unit 1"));
        
        bundle.newPage();
        bundle.add(new Phrase("Foo bar"));
        
        bundle.newPage();
        bundle.add(new Phrase("Baz qux"));
        
        bundle.newPage();
        bundle.add(new Phrase("Page 1 in unit 2"));
        bundle.newPage();
        bundle.add(new Phrase("Page 2 in unit 2"));
        bundle.newPage();
        bundle.add(new Phrase("Page 3 in unit 2"));
        
        bundle.newPage();
        bundle.add(new Phrase("Lorem ipsum"));
        bundle.newPage();
        bundle.add(new Phrase("Dolor sit amet"));
        
        bundle.close();
        writer.close();
        
        
        individualFiles = new HashMap<String, ByteArrayOutputStream>(2);
        
        dest = mock(Destination.class);
        when( dest.getOutput(any(Unit.class)) ).then(new Answer<OutputStream>() {
            public OutputStream answer(InvocationOnMock invocation) throws Throwable {
                Unit unit = invocation.getArgumentAt(0, Unit.class);
                
                ByteArrayOutputStream out = Mockito.spy(new ByteArrayOutputStream());
                individualFiles.put(unit.getId(), out);
                
                return out;
            }
        });
        
        bundleReader = new PdfReader( bos.toByteArray() );
    }
    
    @Test
    public void testSplit() throws IOException, ParseException, DocumentException {
        List<Unit> units = Arrays.asList(FIRST_UNIT, SECOND_UNIT);
        new Splitter(bundleReader).split(units, dest);
        
        verify(dest).close();
        assertEquals(units.size(), individualFiles.size());
        
        for (final Unit unit : units) {
            ByteArrayOutputStream file = individualFiles.get(unit.getId());
            
            PdfReader unitReader = new PdfReader(file.toByteArray());
            
            int numberOfPages = unitReader.getNumberOfPages();
            int expectedPageCount = (unit.getLastPage()-unit.getFirstPage()) + 1;
            assertEquals(expectedPageCount, numberOfPages);
            
            PdfTextExtractor extractor = new PdfTextExtractor(unitReader);            
            for (int i = 1; i <= numberOfPages; i++) {
                String text = extractor.getTextFromPage(i).trim();
                assertEquals(text, "Page " + i + " in unit " + unit.getId());
            }
        }
    }

    @Test
    public void testTrimPages() throws IOException, ParseException, DocumentException {
        List<Unit> units = Arrays.asList(FIRST_UNIT);
        new Splitter(bundleReader).split(units, dest, 1);
        
        ByteArrayOutputStream file = individualFiles.get("1");
        
        PdfReader unitReader = new PdfReader(file.toByteArray());
        
        assertEquals(1, unitReader.getNumberOfPages());
    }
}

